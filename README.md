# Facebook & Co.Stundent Hackathon

<center>
<img src="https://scontent-waw1-1.xx.fbcdn.net/v/t1.0-9/44402445_2127362900926281_1057829382485180416_n.jpg?_nc_cat=106&_nc_ht=scontent-waw1-1.xx&oh=1731144ac5b925ce799862487108aca8&oe=5C49DDAC" width="65%">
</center>

## Installation notes

For running this server need to be sure that you have installed:

- [__enchant__](https://github.com/AbiWord/enchant) and python wrapper - [__pyenchant__](https://github.com/rfk/pyenchant)
- [__tesseract__](https://github.com/tesseract-ocr/tesseract)
- [__pytorch__](https://pytorch.org/)
- [__nms__](https://pypi.org/project/nms/)
- [__flask__](http://flask.pocoo.org/)

-------------------------------------------------------------------------------

__Tesseract__ can be installed on mac os using homebrew (`brew install tesseract`) or on a linux using through apt-get (`sudo apt-get install tesseract`).

__Pyenchant__ use gnu enchant. You can install it on mac os using homebrew (`brew install enchant`) or on a linux through apt-get (`sudo apt-get install enchant`).

__Pytorch__ guides for installation can be found on official web site. 

__Non Maximal Suppression__ can be installed using `pip` (`pip install npm`).

# Runing

`server.py` has a few parameters:

- `--host` - IP address, default - `localhost`
- `--port` - port number, default - `8888`
- `--cache` - folder for storing cache (it will be created if it's not exist), default - `cache`
- `--delay` - image loading time delay (in seconds), default - `0`
- `--backend` - backend to use (`tesseract` or `torch`), default - `tesseract`

## Notes

Basically you can run server with default parameters `python3 server.py`.

Need to note that `pytorch` backend ia slower but recognizes more text than `tesseract`.

This code tested on __python 3.6__ and __3.7__.