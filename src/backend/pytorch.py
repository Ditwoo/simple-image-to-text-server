import cv2
import time
import torch
import torchvision.transforms as transforms
from PIL import Image
import numpy as np
from nms import nms
from nets import load_pretrained
from nets.utils import StrLabelConverter
from interface import BackendInterface


class ResizeNormalize(object):
    def __init__(self, size, interpolation=Image.BILINEAR):
        self.size = size
        self.interpolation = interpolation
        self.toTensor = transforms.ToTensor()

    def __call__(self, img):
        img = img.resize(self.size, self.interpolation)
        img = self.toTensor(img)
        img.sub_(0.5).div_(0.5)
        return img


def decode(scores, geometry, confidence_threshold):
    (numRows, numCols) = scores.shape[2:4]
    confidences = []
    rects = []  # (x,y,w,h)
    baggage = []

    for y in range(0, numRows):
        scoresData = scores[0, 0, y]
        dTop = geometry[0, 0, y]
        dRight = geometry[0, 1, y]
        dBottom = geometry[0, 2, y]
        dLeft = geometry[0, 3, y]
        anglesData = geometry[0, 4, y]

        for x in range(0, numCols):
            if scoresData[x] < confidence_threshold:
                continue

            confidences.append(float(scoresData[x]))
            (offsetX, offsetY) = (x * 4.0, y * 4.0)
            angle = anglesData[x]

            upperRight = (offsetX + dRight[x], offsetY - dTop[x])
            lowerRight = (offsetX + dRight[x], offsetY + dBottom[x])
            upperLeft = (offsetX - dLeft[x], offsetY - dTop[x])
            lowerLeft = (offsetX - dLeft[x], offsetY + dBottom[x])

            rects.append([
                int(upperLeft[0]),  # x
                int(upperLeft[1]),  # y
                int(lowerRight[0] - upperLeft[0]),  # w
                int(lowerRight[1] - upperLeft[1])  # h
            ])

            baggage.append({
                "offset": (offsetX, offsetY),
                "angle": anglesData[x],
                "upperRight": upperRight,
                "lowerRight": lowerRight,
                "upperLeft": upperLeft,
                "lowerLeft": lowerLeft,
                "dTop": dTop[x],
                "dRight": dRight[x],
                "dBottom": dBottom[x],
                "dLeft": dLeft[x]
            })
    return rects, confidences, baggage


class PytorchBackend(BackendInterface):
    def __init__(self,
                 crnn_model: str,
                 east_model: str,
                 is_cuda: bool=False,
                 alphabet: str='0123456789abcdefghijklmnopqrstuvwxyz',
                 nms_function=nms.felzenszwalb.nms,
                 nms_threshold: float=0.4,
                 min_confidence: float=0.5,
                 scale_height_width: tuple=(320, 320)):
        """

        :param crnn_model: path to pre trained CRNN model
        :param east_model: path to pre trained EAST model
        :param is_cuda: modifier to use CUDA
        :param alphabet: string of characters to use as alphabet in CRNN output decoder
        :param nms_function: function for detecting bounding boxes of text (nms.felzenszwalb.nms, nms.fast.nms, nms.malisiewicz.nms)
        :param min_confidence: confidence level of bounding boxes
        :param scale_height_width: scaling image to some sizes, height % 32 == 0 and width % 32 == 0
        """
        # pytorch part
        self.cuda = is_cuda
        self.text_recognition_model = load_pretrained(crnn_model, is_cuda)
        self.text_recognition_model.eval()
        self.text_recognition_model_converter = StrLabelConverter(alphabet)
        # opencv part
        self.east_model = cv2.dnn.readNet(east_model)
        self._nms_function = nms_function
        self._nms_threshold = nms_threshold
        self._min_confidence = min_confidence
        self._height, self._width = scale_height_width
        self.transformer = ResizeNormalize((100, 32))

    def _text_detection(self, image, verbose: bool=False):
        """
        OpenCV text detection:
            https://bitbucket.org/tomhoag/opencv-text-detection/src/dbe095275253?at=master
        """
        image = cv2.imread(image)
        orig = image.copy()
        (origHeight, origWidth) = image.shape[:2]

        newW, newH = (self._width, self._height)
        ratioWidth = origWidth / float(newW)
        ratioHeight = origHeight / float(newH)

        image = cv2.resize(image, (newW, newH))
        imageHeight, imageWidth = image.shape[:2]

        layerNames = ['feature_fusion/Conv_7/Sigmoid', 'feature_fusion/concat_3']
        net = self.east_model

        blob = cv2.dnn.blobFromImage(image, 1.0,
                                     (imageWidth, imageHeight),
                                     (123.68, 116.78, 103.94),
                                     swapRB=True, crop=False)

        start = time.time()
        net.setInput(blob)
        scores, geometry = net.forward(layerNames)
        end = time.time()
        if verbose:
            print("[INFO] text detection took {:.6f} seconds".format(end - start), flush=True)
        confidence_threshold = self._min_confidence

        # decode the blob info
        rects, confidences, baggage = decode(scores, geometry, confidence_threshold)

        offsets = []
        thetas = []
        for b in baggage:
            offsets.append(b['offset'])
            thetas.append(b['angle'])

        indicies = nms.boxes(rects, confidences,
                             nms_function=self._nms_function,
                             confidence_threshold=confidence_threshold,
                             nsm_threshold=self._nms_threshold)
        indicies = np.array(indicies).reshape(-1)
        out_rects = np.array(rects)[indicies]

        return image, out_rects

    def _get_text_from_box(self, image):
        image = self.transformer(Image.fromarray(image).convert('L'))
        if self.cuda:
            image = image.cuda()
        image = image.view(1, *image.size())
        preds = self.text_recognition_model(image)
        _, preds = preds.max(2)
        preds = preds.transpose(1, 0).contiguous().view(-1)
        preds_size = torch.IntTensor([preds.size(0)])
        # raw_pred = _converter.decode(preds.data, preds_size.data, raw=True)
        sim_pred = self.text_recognition_model_converter.decode(preds.data, preds_size.data, raw=False)

        return sim_pred

    def text_from_image(self, image_path, verbose=False):
        image, rects = self._text_detection(image_path, verbose=verbose)
        text = ' '.join([self._get_text_from_box(image[y:y + h, x:x + w]).strip() for x, y, w, h in rects])
        return text
