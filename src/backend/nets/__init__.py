import torch
from .crnn import CRNN


def load_pretrained(model_path, cuda_available: bool=False):
    model = CRNN(32, 1, 37, 256)
    model.load_state_dict(torch.load(model_path))
    model = model.cuda() if cuda_available else model
    return model
