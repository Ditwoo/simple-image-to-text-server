import subprocess
from interface import BackendInterface


class TesseractBackend(BackendInterface):
    def __init__(self, lang: str='eng'):
        self.lang = lang
        self.command = 'tesseract -l {lang} {image} stdout'

    def text_from_image(self, image_path, verbose: bool=False):
        process = subprocess.Popen(
            self.command.format(lang=self.lang, image=image_path).split(),
            stdout=subprocess.PIPE
        )
        output, errors = process.communicate()
        if verbose:
            print(f'[*] {verbose}')
        return ' '.join(output.decode().split())
