import re
import enchant
from joblib import Parallel, delayed
from abc import ABC, abstractmethod

SPECIAL_CHARACTERS = re.compile(r'[^A-Za-z0-9]+')
UPCASED_WORDS = re.compile(r'[A-Z][a-z]*')


def process_combining(s):
    return ' '.join(UPCASED_WORDS.findall(s)) if UPCASED_WORDS.findall(s) else s


class BackendInterface(ABC):
    EN_US_DICT = enchant.Dict('en_US')

    @abstractmethod
    def text_from_image(self, image_path: str, verbose: bool=False) -> str:
        return ''

    def pretify_text(self, text: str, correct_spelling: bool=False) -> str:
        text = SPECIAL_CHARACTERS.sub(' ', text)
        text = Parallel(n_jobs=-1)(delayed(process_combining)(w) for w in text.split())

        if correct_spelling:
            corrected_output = []
            for item in text:
                suggestion = []
                if not self.EN_US_DICT.check(item):
                    suggestion = self.EN_US_DICT.suggest(item)
                corrected_output.append(suggestion[0] if len(suggestion) else suggestion)
        return ' '.join(text).lower()
