import os
import json
import argparse
from time import sleep, time
from skimage.io import imread, imsave
from pathlib import Path
from flask import Flask, request, abort


ARGUMENT_PARSER = argparse.ArgumentParser()
ARGUMENT_PARSER.add_argument('--host', type=str, default='localhost',
                             help='hosting address')
ARGUMENT_PARSER.add_argument('--port', type=int, default=8888,
                             help='port for listening')
ARGUMENT_PARSER.add_argument('--cache', type=str, default='cache',
                             help='folder used for storing temporary images')
ARGUMENT_PARSER.add_argument('--delay', type=float, default=0,
                             help='time delay (in seconds) for sequential loading of pictures')
ARGUMENT_PARSER.add_argument('--backend', type=str, default='tesseract',
                             help='backend to use (tesseract or torch), default - tesseract')
args = ARGUMENT_PARSER.parse_args()

HOST_ADDR = args.host
PORT = args.port
TIME_DELAY = args.delay
CACHE_FOLDER = Path(args.cache)
BACKEND_OPTION = args.backend.strip().lower()


if not CACHE_FOLDER.is_dir():
    os.makedirs(CACHE_FOLDER)

if BACKEND_OPTION == 'torch':
    from backend.pytorch import PytorchBackend
    pretrained_folder = Path('..') / 'pretrained'
    backend = PytorchBackend(
        crnn_model=str(pretrained_folder / 'crnn.pth'),
        east_model=str(pretrained_folder / 'frozen_east_text_detection.pb')
    )
else:
    from backend.tesseract import TesseractBackend
    backend = TesseractBackend()


app = Flask(__name__)


def _image_to_text(url, correct_spelling: bool=True, verbose: bool=False, clean_cache_after: bool=True):
    try:
        image = imread(url)
        sleep(TIME_DELAY)
        if verbose:
            print(f'[.] Loaded image from {url}', flush=True)
        local_img_name = str(CACHE_FOLDER / url.split('/')[-1])
        imsave(local_img_name, image)
        start = time()
        text = backend.text_from_image(local_img_name)
        computation_time = time() - start
        if verbose:
            print(f'[.] Parsing time - {computation_time:8.3f}s')
        text = backend.pretify_text(text, correct_spelling)
        if clean_cache_after:
            os.remove(local_img_name)
    except:
        text = ''
    return text


def _parse_images(urls, correct_spelling: bool=True):
    image_texts = [_image_to_text(url, correct_spelling, verbose=True) for url in urls]
    return image_texts


def _dict_maker(_id, _text):
    return {'Id': _id, 'Text': _text}


@app.route('/predict', methods=['POST'])
def predict():
    if not request.json:
        abort(400)
    requests_data = json.loads(request.data)
    print(f'[<-] New POST request from {request.remote_addr}')
    if isinstance(requests_data, list):
        texts = _parse_images([r['Url'] for r in requests_data])
        response = [_dict_maker(r['Id'], t) for r, t in zip(requests_data, texts)]
    elif isinstance(requests_data, dict) and 'Id' in requests_data and 'Url' in requests_data:
        response = _dict_maker(requests_data['Id'], _image_to_text(requests_data['Url']))
    else:
        response = '{}'
    print('[->] Sending parsed text')
    return json.dumps(response)


if __name__ == '__main__':
    app.run(host=HOST_ADDR, port=PORT, threaded=True, debug=False)
